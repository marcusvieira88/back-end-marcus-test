var rentalCtrl = require('./rentalController');
var jwt = require('jsonwebtoken');
var express = require('express');
var multer  = require('multer');
var path = require('path');

var router = express.Router(); 
const Response = require('./models/Response');

/* CONFIG UPLOAD IMAGES */
var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, __dirname + process.env.PATH_UPLOAD_IMAGE)
  },
  filename: function (req, file, callback) {
    callback(null, file.originalname + '-' + Date.now() + '.png')
  }
})
var upload = multer({ 
        storage: storage,
        fileFilter: function (req, file, callback) {
            var ext = path.extname(file.originalname);
            if(ext !== '.png') {
                req.fileValidationError = true;
                return callback(null, false,new Error('Only PNG images are allowed.'));
            }
            callback(null, true);
        }       
    });

/*ROUTE WITHOUT AUTHENTICATION */
router.route('/api/authenticate')
 
    /*CREATE TOKEN */
    .post(rentalCtrl.authenticate);

/*MIDDLEWARE TOKEN */
router.use(function(req, res, next) {

    /* ACCESS STATIC FILES */
    var urlsWithoutAutentication = new Array(process.env.URL_WITHOUT_AUTENTICATION);
    if(urlsWithoutAutentication.find(url => req.originalUrl.startsWith(url))){
        next();
    } else {
        if(req.originalUrl.startsWith('/api/')){
        	var token = req.body.token || req.query.token || req.headers['x-access-token'];

     		if(token) {
            	jwt.verify(token, process.env.JWT_SECRET, function(err, decoded) {      
    	            if (err) {
    	            	return res.status(400).send(new Response(400,'',"Error while authenticate token."));    
    	            } else {
    	            	//TOKEN OK 
    	            	req.decoded = decoded;    
    	            	next();
    	            }
            	});
        	} else {
            	return res.status(401).send(new Response(401,'',"Token not sent."));
        	}
        }else{
            return res.status(403).send(new Response(403,'',"Forbidden."));
        }
    }
	
});

/* ROUTES AUTHENTICATED BY TOKEN */
router.route('/api/rentals')
 
 	/*CREATE RENTAL*/
    .post(rentalCtrl.createRental)

    /* GET ALL RENTALS */
    .get(rentalCtrl.getRentals);

router.route('/api/rentals/:rental_id')
 
    /* GET RENTAL BY ID */
    .get(rentalCtrl.getRental)

	/* UPDATE RENTAL BY ID */
	.put(rentalCtrl.updateRental)

	/* DELETE RENTAL BY ID */
	.delete(rentalCtrl.deleteRental);

router.route('/api/images')

    /* UPDATE IMAGES */
    .post(upload.single('rentics-image'),rentalCtrl.uploadImage);

module.exports = router;