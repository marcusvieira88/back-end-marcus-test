class User {
  constructor(id,firstName,surname,email,password) {
    this.id = id;
    this.firstName = firstName;
    this.surname = surname;
    this.email = email;
    this.password = password;
  }
}

module.exports = User;
