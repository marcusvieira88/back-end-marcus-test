var validator = require('express-validator');
var bodyParser = require('body-parser');
var dateFormat = require('dateformat');
var express = require('express'); 
var routes = require('./routes');
var dotenv = require('dotenv');
var csv = require('fast-csv');
var fs = require('fs');
var app = express(); 
dotenv.load();

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

var port = process.env.PORT || 8000; 
app.listen(port);

/* WRITE ERROR LOG */
function writeLog(codeError,messageError){
    var csvStream = csv.format({headers: false,delimiter: ';',rowDelimiter: '\r\n',includeEndRowDelimiter : true}),
    writableStream = fs.createWriteStream("./logs/log.csv",{flags:'a'});
    csvStream.pipe(writableStream);
    csvStream.write({"Date": dateFormat(new Date(),"yyyy-mm-dd'T'HH:MM:ss"),"Response Status": codeError,"Error Message": messageError});
    csvStream.end();
}

/* LISTEN RESPONSE */
function logResponseBody(req, res, next) {
  var oldWrite = res.write,
      oldEnd = res.end;
  var chunks = [];

  res.write = function (chunk) {
    chunks.push(new Buffer(chunk));
    oldWrite.apply(res, arguments);
  };

  res.end = function (chunk) {
    if (chunk)
    	chunks.push(new Buffer(chunk));
    
    if(chunks && res.statusCode != 200){
      try{
  		  var body = Buffer.concat(chunks).toString('utf8');
	 	    var reponse = JSON.parse(body);
        writeLog(response.status,response.message);
      }catch(err){
        writeLog(res.statusCode,err.message);
      }
    }
    oldEnd.apply(res, arguments);
  };
  next();
}
app.use(logResponseBody);

/* REQUEST VALIDATOR */
app.use(validator());

/* DECLARE ROUTES */
app.use('/', routes);

/* STATIC DIRECTORIES */
app.use('/static', express.static(__dirname + '/static'));
app.use('/uploads', express.static(__dirname + '/uploads'));

console.log('Started app - port: ' + port);