var supertest = require("supertest");
var jwt = require('jsonwebtoken');
var should = require("should");
var dotenv = require('dotenv');
dotenv.load();

var server = supertest.agent("http://localhost:8000");

function generateToken(){
	var user = {"id": "59777f3ff000346ae4e60ba5",
			    "firstName": "Hebert",
			    "surname": "Vang",
			    "email": "hebertvang@emtrak.com",
			    "password": "dcec2ffc-8ee8-4eb5-9efb-4d8de74dde44"};
	var jwtToken = jwt.sign(user, process.env.JWT_SECRET, {
        expiresIn : 60*60 //expires in 1 hour
   	});
	return jwtToken;
}

describe("Test token generation",function(){

  it("should return token generated",function(done){
     server
    .post('/api/authenticate')
    .send({
	    "id": "59777f3fdadd1d26cc2a2093",
	    "firstName": "Cornelia",
	    "surname": "Lott",
	    "email": "cornelialott@emtrak.com",
	    "password": "44d6aec6-5d67-47d3-a9ec-2754dba27d6b"
  	})
    .expect("Content-type",/json/)
    .expect(200)
    .end(function(err,res){
      res.status.should.equal(200);
      res.body.message.should.equal("Success.");
      done();
    });
  });

  it("should return error user not found",function(done){
     server
    .post('/api/authenticate')
    .send({
	    "id": "59777f3fdadd1d26cc2a2093",
	    "firstName": "Cornelia",
	    "surname": "Lott",
	    "email": "ERROR_TEST@ERROR_TEST.com",
	    "password": "44d6aec6-5d67-47d3-a9ec-2754dba27d6b"
  	})
    .expect("Content-type",/json/)
    .expect(404)
    .end(function(err,res){
      res.status.should.equal(404);
      res.body.message.should.equal("User not found.");
      done();
    });
  });

   it("should return error data access invalid",function(done){
     server
    .post('/api/authenticate')
    .send({
	    "id": "59777f3fdadd1d26cc2a2093",
	    "firstName": "Cornelia",
	    "surname": "Lott",
	    "email": "cornelialott@emtrak.com",
	    "password": "WRONG_PASSWORD"
  	})
    .expect("Content-type",/json/)
    .expect(401)
    .end(function(err,res){
      res.status.should.equal(401);
      res.body.message.should.equal("Data Access Invalid.");
      done();
    });
  });

});

describe("Create Rental",function(){

  it("should return rental created",function(done){
     server
    .post('/api/rentals')
    .set('x-access-token',generateToken())
    .send({
	    "id": "59777c092a1248e5fd42ab48",
	    "address": {
	      "street": "Rose Street",
	      "houseNumber": 19,
	      "city": "Coral",
	      "zipCode": 10384
	    },
	    "numberRooms": 4,
	    "area": 63,
	    "views": 54
	})
    .expect("Content-type",/json/)
    .expect(200)
    .end(function(err,res){
      res.status.should.equal(200);
      done();
    });
  });

  it("should return error houseNumber is required",function(done){
     server
    .post('/api/rentals')
    .set('x-access-token',generateToken())
    .send({
	    "id": "59777c092a1248e5fd42ab48",
	    "address": {
	      "street": "Rose Street",
	      //"houseNumber": 19,
	      "city": "Coral",
	      "zipCode": 10384
	    },
	    "numberRooms": 4,
	    "area": 63,
	    "views": 54
	})
    .expect("Content-type",/json/)
    .expect(400)
    .end(function(err,res){
      res.status.should.equal(400);
      res.body.data[0].param.should.equal("address.houseNumber");
      res.body.data[0].msg.should.equal("HouseNumber is required.");
    done();
    });
  });

  it("should return error token not sent",function(done){
     server
    .post('/api/rentals')
    //.set('x-access-token',generateToken())
    .send({
	    "id": "59777c092a1248e5fd42ab48",
	    "address": {
	      "street": "Rose Street",
	      "houseNumber": 19,
	      "city": "Coral",
	      "zipCode": 10384
	    },
	    "numberRooms": 4,
	    "area": 63,
	    "views": 54
	})
    .expect("Content-type",/json/)
    .expect(401)
    .end(function(err,res){
      res.status.should.equal(401);
      res.body.message.should.equal("Token not sent.");
    done();
    });
  });
});

describe("Update Rental",function(){

  it("should return rental updated",function(done){
     server
    .put('/api/rentals/59777c093427d41d877a6e91')
    .set('x-access-token',generateToken())
    .send({
	    "id": "59777c093427d41d877a6e91",
	    "address": {
	      "street": "Sunnyside Court",
	      "houseNumber": 15,
	      "city": "Idledale",
	      "zipCode": 10956
	    },
	    "numberRooms": 4,
	    "area": 45,
	    "views": 57
  	})
    .expect("Content-type",/json/)
    .expect(200)
    .end(function(err,res){
      res.status.should.equal(200);
      done();
    });
  });

  it("should return error zipcode is required",function(done){
     server
    .put('/api/rentals/59777c093427d41d877a6e91')
    .set('x-access-token',generateToken())
    .send({
	    "id": "59777c093427d41d877a6e91",
	    "address": {
	      "street": "Sunnyside Court",
	      "houseNumber": 15,
	      "city": "Idledale"
	      //"zipCode": 10956
	    },
	    "numberRooms": 4,
	    "area": 45,
	    "views": 57
  	})
    .expect("Content-type",/json/)
    .expect(400)
    .end(function(err,res){
      res.status.should.equal(400);
      res.body.data[0].param.should.equal("address.zipCode");
      res.body.data[0].msg.should.equal("ZipCode is required.");
    done();
    });
  });
});

describe("Delete Rental",function(){

  it("should return rental deleted",function(done){
     server
    .delete('/api/rentals/59777c09866aa0dcd7c14fff')
    .set('x-access-token',generateToken())
    .expect("Content-type",/json/)
    .expect(200)
    .end(function(err,res){
      res.status.should.equal(200);
      done();
    });
  });

  it("should return error rental not found",function(done){
     server
    .delete('/api/rentals/59777c09866aa0dcd7cXXXXX')
    .set('x-access-token',generateToken())
    .expect("Content-type",/json/)
    .expect(404)
    .end(function(err,res){
      res.status.should.equal(404);
      res.body.message.should.equal("Rental not found.");
    done();
    });
  });
});

describe("Get Rental",function(){

  it("should return all rentals",function(done){
     server
    .get('/api/rentals')
    .set('x-access-token',generateToken())
    .expect("Content-type",/json/)
    .expect(200)
    .end(function(err,res){
      res.status.should.equal(200);
      done();
    });
  });

  it("should return one rental",function(done){
     server
    .get('/api/rentals/59777c091f06ce82426ab88d')
    .set('x-access-token',generateToken())
    .expect("Content-type",/json/)
    .expect(200)
    .end(function(err,res){
      res.status.should.equal(200);
      res.body.message.should.equal("Success.");
    done();
    });
  });

  it("should return error rental not found",function(done){
     server
    .get('/api/rentals/59777c091f06ce82426XXXXX')
    .set('x-access-token',generateToken())
    .expect("Content-type",/json/)
    .expect(204)
    .end(function(err,res){
      res.status.should.equal(404);
      res.body.message.should.equal("Rental not found.");
    done();
    });
  });
});

describe("Upload Images",function(){

  it("should return upload PNG image success",function(done){

    server
    .post('/api/images')
    .set('x-access-token',generateToken())
    .set('Content-Type', 'multipart/form-data')
    .attach('rentics-image', __dirname + '/imageTest/imagePngType.png')
    .expect(200)
    .end(function(err,res){
        res.status.should.equal(200);
		res.body.message.should.equal("Image upload success.");
      done();
    });
  });

  it("should return token not sent error",function(done){

    server
    .post('/api/images')
    //.set('x-access-token',generateToken())
    .set('Content-Type', 'multipart/form-data')
    .attach('rentics-image', __dirname + '/imageTest/imagePngType.png')
    .expect(401)
    .end(function(err,res){
        res.status.should.equal(401);
		res.body.message.should.equal("Token not sent.");
      done();
    });
  });

  it("should return upload JPG image error",function(done){

    server
    .post('/api/images')
    .set('x-access-token',generateToken())
    .set('Content-Type', 'multipart/form-data')
    .attach('rentics-image', __dirname + '/imageTest/imageJpgType.jpg')
    .expect(400)
    .end(function(err,res){
        res.status.should.equal(400);
        res.body.message.should.equal("Only PNG images are allowed.");
      done();
    });
  });
});

describe("Download Static Files",function(){

  it("should return static file without token",function(done){

    server
    .get('/static/example.pdf')
    .expect(200)
    .end(function(err,res){
        res.status.should.equal(200);
      done();
    });
  });

  it("should return error access not authorized",function(done){

    server
    .get('/uploads/XXXX.png')
    .expect(403)
    .end(function(err,res){
        res.status.should.equal(403);
        res.body.message.should.equal("Forbidden.");
      done();
    });
  });
});


