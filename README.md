# Code Challenge

This app was built to respond to the code challenge of Disponio.

## Frameworks

The frameworks used during development are:

* [express](https://expressjs.com/en/4x/api.html) - Web framework
* [express-validator](https://github.com/ctavan/express-validator) - Request Validator 
* [multer](https://github.com/expressjs/multer) - Upload image
* [dotenv](https://github.com/motdotla/dotenv) - Loads environment variables
* [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken) - Token Authentication
* [fast-csv](https://github.com/C2FO/fast-csv) - CSV parsing and formatting
* [dateformat](https://github.com/felixge/node-dateformat) - Format Dates
* [mocha](https://github.com/mochajs/mocha) - Test framework
* [supertest](https://github.com/visionmedia/supertest) - Test Http
* [should](https://shouldjs.github.io/) - Test framework

## Install

```
npm install
```

## Start

```
node app.js
```

## Tests

```
npm test
```

## Api

| Method | Resource 		 | Description               |
|--------|-------------------|---------------------------|
| POST	 | api/authenticate/ | create a new token 	     | 
| GET	 | api/rentals/	     | return all rentals	     |
| POST	 | api/rentals/	     | create a new rental	     | 
| GET	 | api/rentals/{id}  | return specified rental	 |
| PUT	 | api/rentals/{id}  | update specified rental	 |
| DELETE | api/rentals/{id}  | delete specified rental	 |
| POST	 | api/images/       | store a image into server |
