var jwt = require('jsonwebtoken');
var express = require('express');
var multer  = require('multer');
var path = require('path');
var fs = require('fs');
var app = express();

/* LOAD JSON */
var listOfRentals = JSON.parse(fs.readFileSync('./sources/rentals.json', 'utf8'));
var listOfUsers = JSON.parse(fs.readFileSync('./sources/users.json', 'utf8'));

var upload = multer().single('rentics-image');
const Response = require('./models/Response');

function sendResponse(res,json){
	if(json){
	    res.status(200).send(new Response(200,json,"Success."));
	}else{
		res.status(404).send(new Response(404,'',"Rental not found."));
	}
}

function validateFields(req){
	req.checkBody("id", "Id is required.").notEmpty();
	req.checkBody("address.street", "Street is required.").notEmpty();
	req.checkBody("address.houseNumber", "HouseNumber is required.").notEmpty();
	req.checkBody("address.city", "City is required.").notEmpty();
	req.checkBody("address.zipCode", "ZipCode is required.").notEmpty();
	req.checkBody("numberRooms", "NumberRooms is required.").notEmpty();
	req.checkBody("area", "Area is required.").notEmpty();
	return req.validationErrors();
}

module.exports = {
	authenticate : function(req, res) {
		var user = listOfUsers.find(user => user.email === req.body.email);
		if(!user){
			res.status(404).send(new Response(404,'',"User not found."));
		}else if(user.password != req.body.password){
			res.status(401).send(new Response(401,'',"Data Access Invalid."));
		}else {
			var jwtToken = jwt.sign(user, process.env.JWT_SECRET, {
                expiresIn : 60*60*24 //expires in 24 hours
            });
            res.status(200).send(new Response(200,{token : jwtToken},"Success."));
		}
	},
	getRentals : function(req, res) {
		sendResponse(res,listOfRentals);
    },
    createRental : function(req, res) {
    	var errors = validateFields(req);
		if (errors) {
			res.status(400).send(new Response(400,errors,"Bad request."));
  			return;
  		}
    	sendResponse(res,req.body);
 	},
	getRental : function(req, res) {
		var rental = listOfRentals.find(rental => rental.id === req.params.rental_id);
    	sendResponse(res,rental);
	},
	updateRental : function(req, res) {
		var errors = validateFields(req);
		if (errors) {
			res.status(400).send(new Response(400,errors,"Bad request."));
  			return;
  		}
		var rental = listOfRentals.find(rental => rental.id === req.params.rental_id);
		sendResponse(res,rental);
	},
	deleteRental : function(req, res) {
		var rental = listOfRentals.find(rental => rental.id === req.params.rental_id);
		sendResponse(res,rental);
	},
	uploadImage : function (req, res) {
  		upload(req, res, function (err) {
  	    	if (req.fileValidationError) {
	       		res.status(400).send(new Response(400,'','Only PNG images are allowed.'));
	      		return;
	    	}
     		res.status(200).send(new Response(200,'',"Image upload success."));
  		})
	}
}